#docker run -d --name realmd --net=host --restart always -p 3724:3724 -v ~/mangos/zero/etc/:/mangos/etc realmd:latest
docker run -d --name mangosd \
	--restart always \
	--net=host \
	-it \
	-v ~/mangos/zero/etc/:/mangos/etc \
	-v ~/mangos/zero/data:/mangos/data \
	dockercontainer-mangosd:latest
