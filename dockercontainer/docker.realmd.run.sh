#docker run -d --name realmd --net=host --restart always -p 3724:3724 -v ~/mangos/zero/etc/:/mangos/etc realmd:latest
sudo docker run -d --name realmd \
	--restart always \
	-p 3724:3724 \
	--net=host \
	-v ~/mangos/zero/etc/:/mangos/etc \
	dockercontainer-realmd:latest
